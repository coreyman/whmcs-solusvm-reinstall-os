<?php
require_once("dbconnect.php"); //need these requires for whmcs internal api
require_once("includes/functions.php"); 
require_once("includes/clientareafunctions.php");
$operatingSystems = array(//need to get these from whmcs
	array('centos-5.0-x86', 'CentOS 5 32bit'),
	array('centos-5-x86_64-devel', 'CentOS 5 64bit'),
	array('debian-5.0-x86', 'Debian 5.0 32bit'),
	array('debian-5.0-x86_64', 'Debian 5.0 64bit'),
	array('fedora-13-x86', 'Fedora 13 32bit'),
	array('fedora-13-x86_64', 'Fedora 13 64bit'),
	array('ubuntu-12.04-x86_64', 'Ubuntu 12.04 64bit'),
	array('suse-11.4-x86', 'Suse 11.4 32bit'),
	array('suse-11.4-x86_64', 'Suse 11.4 64bit'),
	array('ubuntu-12.04-x86', 'Ubuntu 12.04 32bit'),
	array('ubuntu-11.10-x86', 'Ubuntu 11.10 32bit'),
	array('ubuntu-11.10-x86_64', 'Ubuntu 11.10 64bit'),
	array('arch-2010.05-i686-minimal', 'Arch Linux 32bit'),
	array('arch-2010.05-x86_64-minimal', 'Arch Linux 64bit'),
	array('debian-6.0-x86', 'Debian 6.0 32bit'),
	array('debian-6.0-x86_64', 'Debian 6.0 64bit'),
	array('centos-6-x86', 'CentOS 6 32bit'),
	array('centos-6-x86_64', 'CentOS 64bit'),
	array('ubuntu-11.04-x86', 'Ubuntu 11.04 32bit'),
	array('ubuntu-11.04-x86_64', 'Ubuntu 11.04 64bit'),
	array('fedora-15-x86_64', 'Fedora 15 64bit'),
	array('fedora-15-x86', 'Fedora 15 32bit')
);

if(isset($_GET['id'])){//need to sanitize this
$values['serviceid'] = $_GET['id'];

$results = localAPI(getclientsproducts,$values,coreyman);//get the vserverid from whmcs

foreach ($results['products']['product'][0]['customfields']['customfield'] as $customfield){//loop through all custom fields looking for vserverid
	if($customfield['name'] == 'vserverid'){ $vserver = $customfield['value']; }
}

}
if(isset($_POST['osChoice'])){//need to sanitize this too ---- they want to reload the os now
	$url = 'https://solusvm.yourdomaingoeshere.com:5656/api/admin';
	$postfields["id"] = "M6c2nYHYz6wLqTbXREVQ";//need to get this from whmcs
	$postfields["key"] = "cgutP5FaRrNuF2ibPI7c";//need to get this from whmcs
	$postfields["action"] = "vserver-rebuild";
	$postfields["vserverid"] = $vserver; 
	$postfields["template"] = $_POST['osChoice'];
	
	// Send the query to the solusvm master 
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url . "/command.php");
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_TIMEOUT, 20);
	curl_setopt($ch, CURLOPT_FRESH_CONNECT, 1);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array("Expect:"));
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);
	$data = curl_exec($ch);
	curl_close($ch);
	
	//need to update the 'operating system' of the product with whmcs api
	// it is in $results['products']['product'][0]['configoptions']['configoption']['option'] => Operating System
	//['value'] => Debian 5.0 32bit
}
?>
<html>
<head>
	<style type="text/css">	body,td,th{color:#666;font-family:Lucida Sans Unicode, Lucida Grande, sans-serif;font-size:12px;}
							input,select,textarea{color:#666;font-family:Lucida Sans Unicode, Lucida Grande, sans-serif;font-size:11px;margin:0;padding:0px;}
	</style>
</head>
<body>
	<br>
	<div align="center">
		<?php if(isset($_POST['osChoice'])){
		echo '<span style="font-weight: bold; color: #900">'.$data.'</span>';
		}
		?>
		<form method="post" action="reinstallOS.php?id=<?php echo $_GET['id']; ?>">New Operating System: 
			<select name="osChoice">
			<?php
			foreach($operatingSystems as $os){
			echo '<option value="'.$os[0].'">'.$os[1].'</option>';
			}
			?>
			</select>
			<input type="submit" value="Reload OS"/>
		</form><br><br>
		<input type="button" value="Close Window" onClick="window.close()"/>
	</div>
</body>
</html>